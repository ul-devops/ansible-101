# Ansible

## Requirements

* python
* ansible-playbook

_NOTE_: `ansible-playbook` peut-être installé via `python`:

```shell
$ python -V
Python 3.9.5
$ pip install --user ansible
$ ansible-playbook --version
ansible-playbook [core 2.11.4] 
  config file = None
  configured module search path = ['/home/mathieu/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /home/mathieu/.local/lib/python3.9/site-packages/ansible
  ansible collection location = /home/mathieu/.ansible/collections:/usr/share/ansible/collections
  executable location = /home/mathieu/.local/bin/ansible-playbook
  python version = 3.9.5 (default, Jul 19 2021, 20:12:45) [GCC 10.3.0]
  jinja version = 3.0.1
  libyaml = True
```

## Ansible variables

ref: https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html

Le but c'est d'identifier les éléments qu'on peut variabiliser - afin de rentre l'exécution du playbook plus flexible.

Example:

```yaml
$ cat ./playbook.yml
---
- hosts: 127.0.0.1
  name: hello
  tasks:
    - name: print-hello
      debug:
        msg: "hello {{ user }}"
$ cat ./variables.yml
---
user: isf
$ ansible-playbook ./playbook.yml -e @./variables.yml
[WARNING]: No inventory was parsed, only implicit localhost is available
[WARNING]: provided hosts list is empty, only localhost is available. Note that
the implicit localhost does not match 'all'

PLAY [hello-world] *************************************************************

TASK [Gathering Facts] *********************************************************
ok: [127.0.0.1]

TASK [print-hello] *************************************************************
ok: [127.0.0.1] => {
    "msg": "hello isf"
}

PLAY RECAP *********************************************************************
127.0.0.1                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

:warning: C'est du Python derrière, donc on peut définir des variables un peu plus complexe:

```shell
$ cat ./playbook.yml
---
- hosts: 127.0.0.1
  name: hello-world
  tasks:
    - name: print-hello
      debug:
        msg: "hello {{ creds['username'] }}"
$ cat ./variables.yml 
---
creds:
  username: mathieu
  password: toto1234
```

_NOTE_:
  * les variables peuvent être chargées via `vars_files` dans le playbook
  * je recommande d'avoir une valeur par défaut pour chaque variable afin d'éviter une panique `{{ user | default("toto") }}`
  * il y a une hiérarchie dans l'ordre de chargement des variables (voir la [ref](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html))

## Ansible vault

ref: https://docs.ansible.com/ansible/latest/user_guide/vault.html

Stocker des variables c'est bien, mais certaines valeurs peuvent être sensibles (mot de passe, certificat, ID, etc.) - `ansible-vault` permet de chiffrer (donc dans les deux sens) des variables.

```shell
$ cat ./playbook.yml
---
- hosts: 127.0.0.1
  name: hello-world
  tasks:
    - name: print-hello
      debug:
        msg: "hello {{ creds['username'] }} with password: {{ password }}"
$ ansible-vault create ./vault.yml
New Vault password:
Confirm New Vault password:
$ file ./vault.yml
./vault.yml: Ansible Vault, version 1.1, encryption AES256
$ echo your-vault-password > ./password
$ ansible-playbook --vault-password-file ./password -e @./variables.yml -e @./vault.yml ./playbook.yml
PLAY [hello-world] *********************************************************************************************************************************************

TASK [Gathering Facts] *****************************************************************************************************************************************
ok: [127.0.0.1]

TASK [print-hello] *********************************************************************************************************************************************
ok: [127.0.0.1] => {
    "msg": "hello mathieu with password: toto1234"
}

PLAY RECAP *****************************************************************************************************************************************************
127.0.0.1                  : ok=2    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

_Note_:
  * le mot de passe du vault peut-être passé via un fichier - donc facile à automatiser afin de le récupérer d'une source sûr (consul, etc.)
  * le mot de passe peut-être récupérer via un script
  * `./vault.yml` peut-être versionné

## Ansible roles

ref: https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html

L'intérêt d'un rôle est comparable à celui d'une fonction dans un programme. Dans notre cas, on pourrait penser à deux rôles:
  * gestion des paquets via APT
  * gestion des templates (fail2ban, etc.)
  * ...

_Note_: les rôles peuvent être partagé publiquement et administré via `ansible-galaxy` (gestionnaire de rôles)

Pour créer un rôle avec un boilerplate:

```shell
$ ansible-galaxy init apt
$ tree .
.
└── apt
    ├── defaults
    │   └── main.yml
    ├── files
    ├── handlers
    │   └── main.yml
    ├── meta
    │   └── main.yml
    ├── README.md
    ├── tasks
    │   └── main.yml
    ├── templates
    ├── tests
    │   ├── inventory
    │   └── test.yml
    └── vars
        └── main.yml
```

_Note_:
  * task/main.yml: on définit les tasks ici
  * defaults/main.yml: on définit les variables du rôle ici.

## Ansible tags

ref: https://docs.ansible.com/ansible/latest/user_guide/playbooks_tags.html

L'objectif des tags est d'articuler les roles et les variables autour d'eux.

Example de tags:
  * type d'application: `front`, `back`, `middleware`, ...
  * environment: `staging`, `prod`, `dev`, ...

Il n'y a pas de rêgles si ce n'est d'essayer d'être cohérent au niveau des noms.

_Note_: 
  * l'utilisation des tags permet d'être granulaire dans son controle de déploiement
  * les tags sont accessibles au moment du runtime via `ansible_run_tags`

